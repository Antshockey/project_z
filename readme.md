#Project Z

##Introduction
Project Z is basically an IRC chat service with a game world that is randomly
generated for people to explore.
The game will contain forests, oceans, deserts and etc.

##Technologies
###Front End
* HTML
* SASS? (Current example written in plain CSS)

###Backend
* Erlang (With Cowboy for the web server)
* Mnesia

##Development Process
A remote called develop is the main branch to do code work on.
Anything on the master branch will be live.
**Please do any development work on the develop branch**
A merge onto master will be done with a release so that we can create an accurate release note.
